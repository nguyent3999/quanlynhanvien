function layThongTinNhanVien() {
  let taiKhoan = document.querySelector("#tknv").value.trim();
  let hoTen = document.querySelector("#name").value.trim();
  let email = document.querySelector("#email").value.trim();
  let matKhau = document.querySelector("#password").value.trim();
  let ngayLam = document.querySelector("#datepicker").value.trim();
  let luongCB = document.querySelector("#luongCB").value.trim();
  let chucVu = document.querySelector("#chucvu").value.trim();
  let gioLam = document.querySelector("#gioLam").value.trim();
  return (nv = new NhanVien(
    taiKhoan,
    hoTen,
    email,
    matKhau,
    ngayLam,
    luongCB,
    chucVu,
    gioLam
  ));
}

function renderDSNV(list) {
  let contentHTML = "";

  for (let i = 0; i < list.length; i++) {
    let createTr = `<tr>
    <td>${list[i].taiKhoan}</td>
    <td>${list[i].hoTen}</td>
    <td>${list[i].email}</td>
    <td>${list[i].ngayLam}</td>
    <td>${list[i].chucVu}</td>
    <td>${list[i].tongLuong()}</td>
    <td>${list[i].loaiNhanVien()}</td>
    <td onclick = 'suaNv("${
      list[i].taiKhoan
    }")' data-toggle="modal" data-target="#myModal" class='button btn-primary'>Change</td>
    <td onclick='xoaNv("${
      list[i].taiKhoan
    }")'  class='button btn-danger'>Delete</td>
    </tr>`;
    contentHTML += createTr;
  }
  document.querySelector("#tableDanhSach").innerHTML = contentHTML;
}

function showThongTinLenForm(nv) {
  document.querySelector("#tknv").value = nv.taiKhoan;
  document.querySelector("#tknv").disabled = true;
  document.querySelector("#name").value = nv.hoTen;
  document.querySelector("#email").value = nv.email;
  document.querySelector("#password").value = nv.matKhau;
  document.querySelector("#datepicker").value = nv.ngayLam;
  document.querySelector("#luongCB").value = nv.luongCB;
  document.querySelector("#chucvu").value = nv.chucVu;
  document.querySelector("#gioLam").value = nv.gioLam;
}

function timIndexNv(taiKhoanNv) {
  return DSNV.findIndex(function (nv) {
    return nv.taiKhoan == taiKhoanNv;
  });
}

function resetForm() {
  document.querySelector('[role = "form"]').reset();
}

function phanLoaiNhanVien(nv, value) {
  if (value == "Xuất Sắc") {
    dsnvXuatSac.push(nv);
  } else if (value == "Giỏi") dsnvGioi.push(nv);
  else if (value == "Khá") dsnvKha.push(nv);
  else if (value == "Trung Bình") dsnvTrungBinh.push(nv);
}
// resetThongBaoError
function resetThongBaoError() {
  document.querySelectorAll(".sp-thongbao").forEach(function (sp) {
    sp.innerText = "";
    sp.style.display = "none";
  });
}
