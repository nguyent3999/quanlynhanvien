function kiemTra(value, idError, textContent) {
  if (!value) {
    document.querySelector(idError).style.display = 'inline-block';
    document.querySelector(idError).innerText = textContent;
    return false;
  } else {
    document.querySelector(idError).style.display = 'none';
    document.querySelector(idError).innerText = '';
    return true;
  }
}

function kiemTraRong(value, idError) {
  return kiemTra(value, idError, 'Trường này không để trống');
}

function kiemTraTrungTaiKhoan(value, idError) {
  if (timIndexNv(value) !== -1) {
    document.querySelector(idError).style.display = 'inline-block';
    document.querySelector(idError).innerText = 'Tài khoản bị trùng';
    return false;
  } else {
    document.querySelector(idError).style.display = 'none';
    document.querySelector(idError).innerText = '';
    return true;
  }
}

function kiemTraTaiKhoanHopLe(value, idError) {
  const re = /^[a-z0-9]{4,6}$/;
  let isUsername = re.test(value);
  return kiemTra(
    isUsername,
    idError,
    'Tài khoản tối đa 4 - 6 ký số, viết thường'
  );
}

function kiemTraTen(value, idError) {
  const re = /^([^0-9]*)$/;
  let isName = re.test(value);
  return kiemTra(isName, idError, 'Tên nhân viên phải là chữ');
}

function kiemTraEmail(value, idError) {
  const re =
    /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;

  let isEmail = re.test(value);
  return kiemTra(isEmail, idError, 'Email không hợp lệ');
}

function kiemTraMatKhau(value, idError) {
  const re = /^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$/;
  let isPassword = re.test(value);
  return kiemTra(
    isPassword,
    idError,
    'mật Khẩu từ 6-10 ký tự (chứa ít nhất 1 ký tự số, 1 ký tự in hoa, 1 ký tự đặc biệt), không để trống'
  );
}

function kiemTraNgay(value, idError) {
  const re =
    /^((0?[1-9]|1[012])[- /.](0?[1-9]|[12][0-9]|3[01])[- /.](19|20)?[0-9]{2})*$/;
  let isDate = re.test(value);
  return kiemTra(isDate, idError, 'Nhập sai ngày, định dạng mm/dd/yyyy');
}

function kiemTraLuongCB(value, idError) {
  let isBasicSalary = value >= 1000000 && value <= 20000000 ? true : false;
  return kiemTra(
    isBasicSalary,
    idError,
    'Nhập sai lương, Lương cơ bản 1 000 000 - 20 000 000'
  );
}

function kiemTraChucVu(value, idError) {
  return kiemTra(value, idError, 'Mời nhập chức vụ');
}

function kiemTraGioLam(value, idError) {
  let isWorkHours = value >= 80 && value <= 200 ? true : false;
  return kiemTra(
    isWorkHours,
    idError,
    'Nhập sai giờ làm, số giờ làm trong tháng 80 - 200 giờ'
  );
}
