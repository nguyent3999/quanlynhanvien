// Danh sach nhan Vien
let DSNV = [];
let dsnvXuatSac = [];
let dsnvGioi = [];
let dsnvKha = [];
let dsnvTrungBinh = [];

const updateDsnvTheoLoai = function () {
  // reset array
  dsnvXuatSac = [];
  dsnvGioi = [];
  dsnvKha = [];
  dsnvTrungBinh = [];
  DSNV.forEach((nv) => {
    phanLoaiNhanVien(nv, nv.loaiNhanVien());
  });
};
//lAY DATA TU LOCAL STORAGE
let dataJson = localStorage.getItem("DSNV");
if (dataJson) {
  let dataRaw = JSON.parse(dataJson);
  DSNV = dataRaw.map(function (nv) {
    return new NhanVien(
      nv.taiKhoan,
      nv.hoTen,
      nv.email,
      nv.matKhau,
      nv.ngayLam,
      nv.luongCB,
      nv.chucVu,
      nv.gioLam
    );
  });
  renderDSNV(DSNV);
  updateDsnvTheoLoai();
}

console.log(dsnvXuatSac);
console.log(DSNV);

// SAVE LOCAL STORAGE
function saveLocalStorage() {
  let DSNVjson = JSON.stringify(DSNV);
  localStorage.setItem("DSNV", DSNVjson);
}

function validateTaiKhoan() {
  let isValid = true;
  // Kiem tra tai khoan
  isValid &=
    kiemTraRong(nv.taiKhoan, "#tbTKNV") &&
    kiemTraTaiKhoanHopLe(nv.taiKhoan, "#tbTKNV") &&
    kiemTraTrungTaiKhoan(nv.taiKhoan, "#tbTKNV");
  return isValid;
}
// VALIDATE NHÂN VIÊN
function validateNhanVien() {
  let isValid = true;
  //Kiem Tra hoTen
  isValid &= kiemTraRong(nv.hoTen, "#tbTen") && kiemTraTen(nv.hoTen, "#tbTen");
  //Kiem Tra email
  isValid &=
    kiemTraRong(nv.email, "#tbEmail") && kiemTraEmail(nv.email, "#tbEmail");
  // Kiem Tra mat khau
  isValid &=
    kiemTraRong(nv.matKhau, "#tbMatKhau") &&
    kiemTraMatKhau(nv.matKhau, "#tbMatKhau");
  // Kiem Tra ngay lam
  isValid &=
    kiemTraRong(nv.ngayLam, "#tbNgay") && kiemTraNgay(nv.ngayLam, "#tbNgay");
  // Kiem Tra luong CB
  isValid &=
    kiemTraRong(nv.luongCB, "#tbLuongCB") &&
    kiemTraLuongCB(nv.luongCB, "#tbLuongCB");
  // Kiem Tra Chưc vụ
  isValid &= kiemTraChucVu(nv.chucVu, "#tbChucVu");
  // KiÊM TRA GIỜ LÀM
  isValid &=
    kiemTraRong(nv.gioLam, "#tbGiolam") &&
    kiemTraGioLam(nv.gioLam, "#tbGiolam");
  return isValid;
}

// THÊM NGƯỜI DÙNG
function themNguoiDung() {
  let nv = layThongTinNhanVien();
  console.log(validateTaiKhoan() & validateNhanVien());
  if (validateTaiKhoan() & validateNhanVien()) {
    DSNV.push(nv);
    phanLoaiNhanVien(nv, nv.loaiNhanVien());
    saveLocalStorage();
    renderDSNV(DSNV);
    updateDsnvTheoLoai();
  }
}

// SỬA NHÂN VIÊN, em muốn thử dùng khai báo hàm kiểu nì
const suaNv = function (taiKhoanNv) {
  let index = timIndexNv(taiKhoanNv);
  if (index == -1) return;
  showThongTinLenForm(DSNV[index]);
  document.querySelector("#btnThemNV").disabled = true;
  resetThongBaoError();
};

// RESET FORM, do em muốn thử addEventListener nên dùng cái nì ạ
document.querySelector("#btnThem").addEventListener("click", function () {
  resetForm();
  document.querySelector("#btnThemNV").disabled = false;
  document.querySelector("#tknv").disabled = false;
  resetThongBaoError();
});
// CAP NHAT NHAN VIEN
function capNhatNv() {
  let nvEdit = layThongTinNhanVien();
  if (validateNhanVien()) {
    let index = timIndexNv(nvEdit.taiKhoan);
    DSNV[index] = nvEdit;
    saveLocalStorage();
    renderDSNV(DSNV);
    updateDsnvTheoLoai();
  }
}
//  XÓA NHÂN VIÊN
const xoaNv = function (taiKhoanNv) {
  let index = timIndexNv(taiKhoanNv);
  if (index == -1) return;
  DSNV.splice(index, 1);
  saveLocalStorage();
  renderDSNV(DSNV);
  updateDsnvTheoLoai();
};

// SHOW PASSWORD
document
  .getElementById("eye-toggle")
  .addEventListener("click", function (event) {
    let pwd = document.querySelector("#password");
    let eye = document.querySelector("#eye-toggle");
    if (pwd.type == "password") {
      pwd.type = "text";
      eye.src = "https://i.stack.imgur.com/waw4z.png";
    } else {
      pwd.type = "password";
      eye.src = "https://i.stack.imgur.com/Oyk1g.png";
    }
    event.preventDefault();
  });
// tim nhan vien theo loai
function timNv() {
  let searchName = document
    .querySelector("#searchName")
    .value.trim()
    .toLowerCase();

  if (searchName == "xuất sắc") renderDSNV(dsnvXuatSac);
  else if (searchName == "giỏi") renderDSNV(dsnvGioi);
  else if (searchName == "khá") renderDSNV(dsnvKha);
  else if (searchName == "trung bình") renderDSNV(dsnvTrungBinh);
  else renderDSNV(DSNV);
}
